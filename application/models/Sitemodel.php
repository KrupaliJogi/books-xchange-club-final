<?php
class Sitemodel extends CI_Model{
    
    function insert_signup()
    {
        $php_timestamp = time();
        $create = date("d F Y H:i:s", $php_timestamp);
        $password = md5($this->input->post('password'));
        $data = array(
            'firstname' => $this->input->post('name'),
            'lastname' => $this->input->post('lname'),
            'username' => $this->input->post('username'),
            'password' => $password,
            'emailid' => $this->input->post('email'),
            'createdate'=> $create,
            'updateddate' => $create
        );
        return $this->db->insert('signup', $data);
    }
    
    
    public function insert_login($query,$password){
        
        if($query->result())
        { 
            foreach( $query->result() as $row )
            {
                $pawd =   $row->password;
            }
            if ($password == $pawd)
            {
                return  redirect('dashboard/index');
            }
            else
            {
                $this->session->set_flashdata('error', 'invalid password');
                return redirect('site/sign');
            }
        }
        else {
            $this->session->set_flashdata('error_user', 'invalid username or password');
           // echo "else";exit();
            return $this->load->view('site/signup');
        }
    }
}