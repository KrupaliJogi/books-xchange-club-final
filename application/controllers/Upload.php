<?php
class Upload extends CI_Controller {
    public function __construct() {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('Sitemodel');
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->library("session");
    }
    
    public function index(){//echo"index";exit();
        $this->load->view('layouts/header');
        $this->load->view('upload/create');
    }
}