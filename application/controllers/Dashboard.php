<?php 

class Dashboard extends CI_Controller {
  
        public function __construct() {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('Sitemodel');
        $this->load->helper('url');
    }

    public function index(){
        $this->load->view('layouts/header');
        $this->load->view('dashboard/index');
        $this->load->view('layouts/footer');
    }
}