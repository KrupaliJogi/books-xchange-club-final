<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
    public function __construct() {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('Sitemodel');
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->library("session");
    }
    public function index(){
        $this->load->view('layouts/header');
        $this->load->view('layouts/footer');
    }
    public function sign(){
       //echo"hello";exit();
        $this->load->view('site/signup');
    }
    public function insert_signup(){
        $data = new Sitemodel();
        $data->insert_signup();
        $this->load->view('site/signup');
    }
    public function insert_login(){
       
        $data = new Sitemodel();
        $username= $this->input->post('username');
        $password = md5($this->input->post('password'));
        $this->db->select('password');
        $this->db->where('username',$username);
        $this->db->from('signup');
        $query = $this->db->get();
        $data->insert_login($query,$password);
    
    }
    public function about(){
        $this->load->view('layouts/header');
        $this->load->view('site/aboutus');
      //  $this->load->view('layouts/footer');
    }
    
    public function feedback(){
        $this->load->view('layouts/header');
        $this->load->view('site/feedback');
        //$this->load->view('layouts/footer');
    }
}
