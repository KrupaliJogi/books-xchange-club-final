<link rel="stylesheet" href="../../css/login_style.css" type="text/css" media="all" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
  <div class="info">
    <h1>Welcome to BXC</h1>
  </div>
</div>
<div class="form">
  <div class="thumbnail"><img src="../../images/logo.png" width="100px" height="100px" /></div>
  <form class="register-form"  action="<?php echo base_url('site/insert_signup');?>" method="post">
    <input type="text" class="input_login" placeholder="First Name" name="name" required/>
    <input type="text" class="input_login" placeholder="Last Name" name="lname" required/>	 
	<input type="text" class="input_login" placeholder="Username" name="username" required/>
    <input type="password" class="input_login" placeholder="Password" name="password" required/>
    <input type="text" class="input_login" placeholder="Email-address" name="email" required/>
    <input type="submit" class="button_login" value="create"/>
    <p class="message">Already registered? <a href="#">Sign In</a></p>
  </form>
  <?php if($this->session->flashdata('error_user')){ ?>
  <div style="color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
  ">
   <strong><?php echo $this->session->flashdata('error_user');?></strong>
  </div>
<?php }?>
<?php if($this->session->flashdata('error')){ ?>
  <div style="color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
  ">
   <strong><?php echo $this->session->flashdata('error');?></strong>
  </div>
<?php }?> 
 <form class="login-form"  action="<?php echo base_url('site/insert_login');?>"  method = 'post'>
    <input type="text" placeholder="username" class="input_login" name ="username" required/>
    <input type="password" placeholder="password" class="input_login" name="password" required/>
  <input type="submit" class="button_login" value="Login"/>
    <p class="message">Not registered? <a href="#">Create an account</a></p>
  </form>
</div>
<script type="text/javascript" src="../../js/login.js"></script>