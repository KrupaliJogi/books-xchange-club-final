<script src="../../js/upload.js"></script>
<link rel="stylesheet" href="../../css/upload.css" >
<body>
<div class="container container_upload">
<div class="row">
<form id="bookForm" method="post" class="form-horizontal">
<!-- <div class="col-xs-4 col-xs-offset-1">
<input type="text" class="form-control" name="firstname" placeholder="Firstname" />
</div>
<div class="col-xs-4 col-xs-offset-1">
<input type="text" class="form-control" name="lastname" placeholder="Lastname" />
</div>  -->
<div class="form-group">
<label class="col-xs-1 control-label">Book</label>
<div class="col-xs-4">
<input type="text" class="form-control" name="book[0].title" placeholder="Title" />
</div>
<div class="col-xs-4">
<input type="text" class="form-control" name="book[0].isbn" placeholder="ISBN" />
</div>
<div class="col-xs-2">
<input type="text" class="form-control" name="book[0].price" placeholder="Price" />
</div>
<div class="col-xs-1">
<button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
</div>
</div>

<!-- The template for adding new field -->
<div class="form-group hide" id="bookTemplate">
<div class="col-xs-4 col-xs-offset-1">
<input type="text" class="form-control" name="title" placeholder="Title" />
</div>
<div class="col-xs-4">
<input type="text" class="form-control" name="isbn" placeholder="ISBN" />
</div>
<div class="col-xs-2">
<input type="text" class="form-control" name="price" placeholder="Price" />
</div>
<div class="col-xs-1">
<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
</div>
</div>

<div class="form-group">
<div class="col-xs-5 col-xs-offset-1">
<button type="submit" class="btn btn-default">Submit</button>
</div>
</div>
</form>
</div>
</div>
</body>